#!/usr/bin/env bash
set -e
source install.conf
source utils.sh

#######################################
# Creates an EFI System partition on the given device.
# Globals:
#   None
# Arguments:
#   partition - device path
#######################################
function partition_efi {
    local target="$1"

    log_info "Creating efi on $target"
    mkfs.vfat -F32 "$target"
    mkdir -p /mnt/boot/efi
    mount "$target" /mnt/boot/efi
}

#######################################
# Creates an EXT4 boot partition on the given device.
# Globals:
#   None
# Arguments:
#   partition - device path
#######################################
function partition_boot {
    local target="$1"

    log_info "Creating /boot on $target"
    mkfs.ext4 "$target"
    mkdir -p /mnt/boot
    mount "$target" /mnt/boot
}

#######################################
# Creates a LUKS partition on the given device.
# Globals:
#   None
# Arguments:
#   partition - device path
#   crypt_name - mapper name assigned to the crypt device
# Outputs:
#   Writes opened luks path to stdout
#######################################
function prepare_luks {
    local target="$1"                             # eg /dev/sda3
    local crypt_name=$(format_crypt_name $target) # eg sda3_crypt

    log_info "Creating luks on $target"
    cryptsetup -y -v luksFormat "$target"

    log_info "Opening luks on $target"
    cryptsetup open "$target" "$crypt_name"
}

#######################################
# Creates a BTRFS root filesystem on the given device.
# Globals:
#   BTRFS_MOUNT_OPTS
#   SWAP_FILE_SIZE
# Arguments:
#   partition - device path
#######################################
function partition_root {
    local target="$1"

    log_info "Creating BTRFS filesystem on $target"
    mkfs.btrfs "$target"
    mount -t btrfs "$target" /mnt
    btrfs subvolume create /mnt/@
    btrfs subvolume create /mnt/@home
    btrfs subvolume create /mnt/@swap
    umount -R /mnt

    # Mount subvolumes
    log_info "Mounting btrfs subvolumes"
    mount -t btrfs -o subvol=@,$BTRFS_MOUNT_OPTS "$target" /mnt
    mkdir /mnt/{home,swap}
    mount -t btrfs -o subvol=@home,$BTRFS_MOUNT_OPTS "$target" /mnt/home
    mount -t btrfs -o subvol=@swap,$BTRFS_MOUNT_OPTS "$target" /mnt/swap
}

#######################################
# Creates a swapfile of given size and path
# Globals:
#   BACKUP_DIR
#   ORACLE_SID
# Arguments:
#   file path
#   desired size (must be compliant with fallocate -l)
#######################################
function create_swapfile {
    local target="$1"
    local size="$2"

    log_info "Creating swapfile at /swap/swapfile"
    truncate -s 0 /mnt/swap/swapfile
    chattr +C /mnt/swap/swapfile
    btrfs property set /mnt/swap/swapfile compression none
    fallocate -l $size /mnt/swap/swapfile
    chmod 600 /mnt/swap/swapfile
    mkswap /mnt/swap/swapfile
    swapon /mnt/swap/swapfile
}

#######################################
# Cleanup files from the backup directory.
# Globals:
#   BACKUP_DIR
#   ORACLE_SID
# Arguments:
#   None
#######################################
function partition_disk() {
    local target="$1"
    local partlist=("$target"*)

    log_info "Partitioning $target"

    # Format the disk as GPT with the following paritition scheme:
    # - 1 - 512M        - /boot/efi
    # - 2 - 1GB         - /boot
    # - 3 - remaining   - encrypted /
    sgdisk --zap-all $target
    sgdisk --clear \
        --new=1:0:+512MiB --typecode=1:ef00 \
        --new=2:0:+1GiB --typecode=2:8300 \
        --new=3:0:0 --typecode=3:8300 \
        $target

    local crypt_name=$(format_crypt_name ${partlist[3]})
    local crypt_path="/dev/mapper/$crypt_name"
    prepare_luks ${partlist[3]} $crypt_name
    partition_root $crypt_path
    partition_boot ${partlist[2]}
    partition_efi ${partlist[1]}
    create_swapfile "/mnt/swap/swapfile" $SWAP_FILE_SIZE
}

#######################################
# Install base system packages (pacstrap).
# Globals:
#   NVIDIA_ENABLE
#   VIRTUALIZATION_TOOLS_ENABLE
#   FIREWALL_ENABLE
#   POWERSAVING_ENABLE
# Arguments:
#   None
#######################################
function install_base() {
    log_info "Preparing base system packages"
    local pkg_list=(
        "base"
        "base-devel"
        "linux-firmware"
        "bash-completion"
        "vim"
        "btrfs-progs"
        "git"
        "networkmanager"
        "grub"
        "efibootmgr"
    )

    if [[ $LINUX_ZEN_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Using linux-zen as the kernel for installation target"
        pkg_list+=("linux-zen" "linux-zen-headers")
    elif [[ $LINUX_LTS_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Using linux-lts as the kernel for installation target"
        pkg_list+=("linux-lts" "linux-lts-headers")
    else
        log_info "Using the default kernel for installation target"
        pkg_list+=("linux" "linux-headers")
    fi

    if [[ $NVIDIA_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding nvidia driver packages to installation target"
        pkg_list+=("nvidia-dkms" "nvidia-utils" "nvidia-settings" "nvidia-prime")
    fi

    if [[ $VIRTUALIZATION_TOOLS_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding virtualization packages to installation target"
        pkg_list+=("libvirt" "edk2-ovmf" "ebtables" "dnsmasq" "dmidecode" "podman" "vagrant" "nfs-utils")
    fi

    if [[ $FIREWALL_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding firewall packages to installation target"
        pkg_list+=("firewalld")
    fi

    if [[ $POWERSAVING_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding powersaving packages to installation target"
        pkg_list+=("tlp" "powertop")
    fi

    if [[ $GNOME_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding GNOME packages to installation target"
        pkg_list+=("gnome" "alsa-utils" "gnome-tweaks")

        if [[ $FIREFOX_ENABLE =~ ^[Yy]$ ]]; then
            log_info "Adding Firefox to installation target"
            pkg_list+=("firefox")
        fi
    fi

    if [[ $GNOME_EXTRA_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding GNOME extra packages to installation target"
        pkg_list+=("gnome-extra")
    fi

    if [[ $PIPEWIRE_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding PipeWire packages to installation target"
        pkg_list+=("pipewire" "pipewire-alsa" "pipewire-jack" "pipewire-pulse")
    fi

    if [[ $FLATPAK_ENABLE =~ ^[Yy]$ ]]; then
        log_info "Adding Flatpak to installation target"
        pkg_list+=("flatpak")
    fi

    pacstrap /mnt ${pkg_list[@]}
}

#######################################
# Copies chroot.sh and other required files into the chroot environment
# and executes chroot.sh
# Globals:
#   POWERSAVING_ENABLE
# Arguments:
#   None
#######################################
function exec_chroot() {
    log_info "Preparing chroot"

    if [[ $POWERSAVING_ENABLE =~ ^[Yy]$ ]]; then
        cp services/powertop.service /mnt/etc/systemd/system
    fi

    cp install.conf chroot.sh utils.sh /mnt
    arch-chroot /mnt ./chroot.sh

    # cleanup
    rm /mnt/chroot.sh /mnt/install.conf /mnt/utils.sh
}

function main() {
    partition_disk $TARGET_DISK
    install_base
    log_info "Generating /etc/fstab"
    genfstab -U /mnt >>/mnt/etc/fstab
    exec_chroot
    log_info "Installation complete."
}

main
