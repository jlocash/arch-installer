#!/usr/bin/env bash

function log() {
    echo -e "[$(date)]: $*"
}

function log_info() {
    local cyan="\033[0;36m"
    local nocolor="\033[0m"
    echo -e "${cyan}$(tput bold)[$(date)]: INFO => ${nocolor}$(tput bold)$*$(tput sgr0)"
}

function format_crypt_name {
    echo "$(echo $1 | cut -d '/' -f 3)_crypt"
}
